function mapObject(obj, cb) {
  if (obj.constructor !== Object) {
    return {};
  }
  for (let prop in obj) {
    obj[prop] = cb(obj[prop]);
  }
  return obj;
}

module.exports = mapObject;
