function pairs(obj) {
  if (obj.constructor !== Object) {
    return [];
  }
  let arr = [];
  for (let prop in obj) {
    let pair = [prop, obj[prop]];
    arr.push(pair);
  }
  return arr;
}

module.exports = pairs;
