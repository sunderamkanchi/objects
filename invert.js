function invert(obj) {
  if (obj.constructor !== Object) {
    return [];
  }
  let arr = [];
  for (let prop in obj) {
    let pair = [obj[prop], prop];
    arr.push(pair);
  }
  return arr;
}

module.exports = invert;
