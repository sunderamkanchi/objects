function defaults(obj, defaultProps) {
  if (obj.constructor !== Object && defaultProps.constructor !== Object) {
    return [];
  }
  for (let key in defaultProps) {
    if (obj.hasOwnProperty(key) === false) {
      obj[key] = defaultProps[key];
    }
  }

  return obj;
}

module.exports = defaults;
