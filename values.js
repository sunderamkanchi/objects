function values(obj) {
  if (obj.constructor != Object) {
    return [];
  }
  let arr = [];
  for (let prop in obj) {
    arr.push(obj[prop]);
  }
  return arr;
}

module.exports = values;
