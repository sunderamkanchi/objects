function keys(obj) {
  if (obj.constructor !== Object) {
    return [];
  }
  let arr = [];
  for (let prop in obj) {
    arr.push(prop);
  }
  return arr;
}

module.exports = keys;
